$(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $(".menu").addClass("bg-white nav-shadow");       
            $(".header-logo").addClass("header-color-logo");
            $(".navbar-toggler-icon").addClass("blue-toggler");
        } else {
            $(".menu").removeClass("bg-white nav-shadow");
            $(".header-logo").removeClass("header-color-logo");
            $(".navbar-toggler-icon").removeClass("blue-toggler");
        }
    });
    $(document).on('scroll', function () {
        // if the scroll distance is greater than 100px
        if ($(window).scrollTop() > 100) {
            // do something
            $('.menu').addClass('bg-white');
        }
    });
});
$('.navbar-nav .nav-item').click(function () {
    $('.navbar-nav .nav-item.active').removeClass('active');
    $(this).addClass('active');
});

$(document).on("click", ".nav-item", function () {
    jQuery(".nav-item").closest(".bsnav-mobile").removeClass("in");
    jQuery(".toggler-spring").removeClass("active");
});

$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});



AOS.init();

$(document).ready(function () {
    $("#habitaciones-carousel").owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 20,
        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        nav:true,
        Dots:true,
       
    });
    $("#habitaciones-carousel2").owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 20,
        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        nav:true,
        dots:true,
       
    });
});